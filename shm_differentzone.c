#define _GNU_SOURCE
#include <fcntl.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define SHM_NAME "shm.test_sync_diff_res_shm"

int devnull = 0;
#define EXIT_ON_ERROR(dst, errval, exec_line) do { \
	dst = exec_line ; \
	if (dst == errval) { \
		perror(#exec_line); \
		return 1; \
	} \
} while (0)

int main() {
	long nb_cpu = 0;
	long i = 0;
	time_t start1 = 0;
	pid_t parent_pid = 0;
	cpu_set_t cpu_mask;
	int fd_shm = 0;
	pid_t pid = 0;
	pid_t * res = 0;
	size_t sizeof_res = 0;

	nb_cpu = sysconf(_SC_NPROCESSORS_ONLN);
	start1 = time(NULL) + 10;
	parent_pid = getpid();
	CPU_ZERO(&cpu_mask);

	EXIT_ON_ERROR(fd_shm, -1, shm_open(SHM_NAME, O_RDWR|O_CREAT, 00777));
	sizeof_res = nb_cpu * sizeof(pid_t);
	EXIT_ON_ERROR(devnull, -1, ftruncate(fd_shm, sizeof_res));
	EXIT_ON_ERROR(res, ((void*)-1), mmap(NULL, sizeof_res, PROT_READ|PROT_WRITE, MAP_SHARED, fd_shm, 0));
	EXIT_ON_ERROR(devnull, -1, close(fd_shm));
	fd_shm = 0;

	memset(res, 0, sizeof(pid_t));

	// Fork and assign a CPU per child
	// i begin at 1, because the first process is the parent process
	for (i = 1; i < nb_cpu; ++i) {
		if (fork() == 0) {
			break;
		}
	}
	if (i == nb_cpu) {
		// Father
		i = 0;
	}
	CPU_SET(i, &cpu_mask);
	sched_setaffinity(0, sizeof(cpu_mask), &cpu_mask);

	// Inform about our status
	pid = getpid();
	printf("PID=%d ; OK ; res=%p ; res[%d]=%d\n", pid, res, i, res[i]);

	// Wait until the time to start.
	// Write a `while` to have a real race situation
	while (time(NULL) != start1);


	/////// PAYLOAD
	msync(res, sizeof_res, MS_SYNC);
	res[i] = pid;
	msync(res, sizeof_res, MS_SYNC|MS_INVALIDATE);
	printf("PID=%d ; written and synced\n", pid);
	/////// END PAYLOAD


	if (parent_pid == getpid()) {
		for (i=1 ; i < nb_cpu ; ++i) {
			wait(NULL);
		}
		for (i=0 ; i < nb_cpu ; ++i) {
			printf("PID=%d ; SOOO ; res=%p ; res[%d]=%d\n", pid, res, i, res[i]);
		}
	}
	EXIT_ON_ERROR(devnull, -1, munmap(res, sizeof_res));
	if (parent_pid == getpid()) {
		// clean shm
		shm_unlink(SHM_NAME);
	}
	return 0;
}
