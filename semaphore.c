#define _GNU_SOURCE
#include <fcntl.h>
#include <sched.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define SEMAPHORE_NAME "sem.test_sync_semaphore"

int main() {
	long nb_cpu = 0;
	long i = 0;
	int semvalue = 0;
	time_t start1 = 0;
	pid_t parent_pid = 0;
	sem_t * semaphore = NULL;
	pid_t pid = 0;

	nb_cpu = sysconf(_SC_NPROCESSORS_ONLN);
	start1 = time(NULL) + 10;
	parent_pid = getpid();
	cpu_set_t cpu_mask;
	CPU_ZERO(&cpu_mask);

	semaphore = sem_open(SEMAPHORE_NAME, O_CREAT, 00777, 1);
	if (semaphore == SEM_FAILED) {
		perror("sem_open");
		return 1;
	}

	// Fork and assign a CPU per child
	// i begin at 1, because the first process is the parent process
	for (i = 1; i < nb_cpu; ++i) {
		if (fork() == 0) {
			break;
		}
	}
	if (i == nb_cpu) {
		// Father
		i = 0;
	}
	CPU_SET(i, &cpu_mask);
	sched_setaffinity(0, sizeof(cpu_mask), &cpu_mask);

	// Inform about our status
	pid = getpid();
	printf("PID=%d ; OK ; sem=%p\n", pid, semaphore);

	// Wait until the time to start.
	// Write a `while` to have a real race situation
	while (time(NULL) != start1);


	/////// PAYLOAD

	// Get semaphore in cache
	sem_wait(semaphore);
	sem_post(semaphore);

	sem_wait(semaphore);
	sem_getvalue(semaphore, &semvalue);
	printf("PID = %d ; semvalue = %d\n", pid, semvalue);
	printf("PID = %d ; %d\n", pid, sysconf(_SC_CLK_TCK));
	printf("PID = %d ; will release semaphore in 1 sec\n", pid);
	sleep(1);
	sem_post(semaphore);
	/////// END PAYLOAD


	sem_close(semaphore);
	if (parent_pid == getpid()) {
		for (i=1 ; i < nb_cpu ; ++i) {
			wait(NULL);
		}
		// clean semaphore
		sem_unlink(SEMAPHORE_NAME);
	}
	return 0;
}
