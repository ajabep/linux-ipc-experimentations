#define _GNU_SOURCE
#include <fcntl.h>
#include <mqueue.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define MQ_NAME "/mq.test_sync_mq"
#define J_THRESHOLD 10

int devnull = 0;
#define EXIT_ON_ERROR(dst, errval, exec_line) do { \
	dst = exec_line ; \
	if (dst == errval) { \
		perror(#exec_line); \
		return 1; \
	} \
} while (0)

int main() {
	long nb_cpu = 0;
	long i = 0;
	time_t start1 = 0;
	pid_t parent_pid = 0;
	pid_t pid = 0;
	cpu_set_t cpu_mask;
	mqd_t mqueue = 0;
	struct mq_attr mqueue_attr;
	uint8_t role = 0;
	unsigned int priority = 0;
	uint8_t j = 0;
	ssize_t read_size = 0;
	char * recvmsg = NULL;
	char * msgs[J_THRESHOLD] = {NULL};

	nb_cpu = sysconf(_SC_NPROCESSORS_ONLN);
	start1 = time(NULL) + 10;
	parent_pid = getpid();
	CPU_ZERO(&cpu_mask);

	// init mq & recv buf
	EXIT_ON_ERROR(mqueue, ((mqd_t)-1), mq_open(MQ_NAME, O_RDWR|O_CREAT, 00777, NULL));
	EXIT_ON_ERROR(devnull, -1, mq_getattr(mqueue, &mqueue_attr));
	EXIT_ON_ERROR(recvmsg, NULL, malloc(mqueue_attr.mq_msgsize));

	// Build messages
	for (j = 0 ; j < J_THRESHOLD; ++j) {
		msgs[j] = malloc(4);
		snprintf(msgs[j], 4, "%03d", j);
	}

	// Fork and assign a CPU per child
	// i begin at 1, because the first process is the parent process
	for (i = 1; i < nb_cpu; ++i) {
		if (fork() == 0) {
			break;
		}
	}
	if (i == nb_cpu) {
		// Father
		i = 0;
	}
	CPU_SET(i, &cpu_mask);
	sched_setaffinity(0, sizeof(cpu_mask), &cpu_mask);

	// Determine process's status
	pid = getpid();
	role = i % 2;
	priority = (i + i/4) % 4;
	if (priority % 2) --priority;
	j = J_THRESHOLD;
	printf("PID=%d ; OK ; mqueue=%p ; role=%d ; priority=%d\n", pid, mqueue, role, priority);


	/////// PAYLOAD
	// Wait until the time to start.
	// Write a `while` to have a real race situation
	while (time(NULL) != start1);

	if (role == 0) {
		// Writers
		while (j--) {
			mq_send(mqueue, msgs[j], 4, priority);
		}
		printf("PID=%d ; Everything has been send\n", pid);
	}
	else {
		// Readers
		while (j--) {
			EXIT_ON_ERROR(read_size, -1, mq_receive(mqueue, recvmsg, mqueue_attr.mq_msgsize, &priority));
			printf("PID=%d ; [%d] msg(%d)=%s\n", pid, priority, read_size, recvmsg);
		}
		printf("PID=%d ; Everything has been received\n", pid);
	}
	/////// END PAYLOAD


	mq_close(mqueue);
	if (parent_pid == getpid()) {
		for (i=1 ; i < nb_cpu ; ++i) {
			wait(NULL);
		}
		// clean mq
		mq_unlink(MQ_NAME);
	}
	return 0;
}
