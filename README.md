Linux IPC Experimentation
=========================

When I write C code, I love forking process to parallelize tasks. But, how to
transmit data to the controller ?

We can use pipes, which requires synchronizing its process (due to race
conditions, a child can write during another child is writing into the same
pipe).

Linux give us some some IPC solutions. Mainly the following:

* Shared memory (shm)
	A chunk of memory shared between process. Require semaphore to be
	synchronize to use the same part of memory
* Semaphore
	As mutex, but with process, not only threads
* Message Queues
	A kind of pipes for messages, concerving the integrity, the separation and
	the order of messages. It also implement message priority.

But, are theses technologies (shm & semaphores) same from CPU caches ?

The perfect way to know it is to test it.


Results
-------

Technology    | CPU Cache safe ?
--------------|:---------------------:
Semaphore     | Yes
Shared Memory | Yes/No (details below)

Shared memory is not CPU cache safe. I mean that if you want to use the same
memory part, you need to use another sync technology (as semaphore). Else, if
your child are using another part of memory, you don't need to sync them.

More, Messages Queues are race-safe.


Experiment protocol
---------------------

To test if a technology is safe from CPU cache, we need to create a race
condition, faster than the cache synchronization. To do so, we assign a
different CPU to each process and actively wait the "go" time:

```c
cpu_set_t cpu_mask;
CPU_ZERO(&cpu_mask);
CPU_SET(i, &cpu_mask);
sched_setaffinity(0, sizeof(cpu_mask), &cpu_mask);

while (time() != startTime) ;
```

Then, child and the parent use the IPC created before the fork.


### Semaphore ([`semaphore.c`](semaphore.c))

The payload to test semaphores is that we collect in the process cache by
locking and immediately release the semaphore. Then, we lock the semaphore,
print something and wait 1 second, then release the lock.

#### Expected result

1 process per second print something.


### Shm (same memory zone) ([`shm_samezone.c`](shm_samezone.c))

The payload to test if shm in the same memory place is CPU cache safe is the
following: first, we synchronize the memory region, then we increment an integer
and, finally, we synchronize this memory region.

The latest process print this integer.

#### Expected result

The integer must be equals of the number of CPU threads.


### Shm (different memory zone) ([`shm_differentzone.c`](shm_differentzone.c))

The goal of this test is to test if CPU cache will rewrite a contiguous memory
zone.

The payload to test if shm in different memory place is CPU cache safe is the
following: first, we synchronize the memory region, then we set a integer in a
zone, the integer and the zone being dependent of the process, finally, we
synchronize this memory region.

The latest process print all zones.

To have different zone, we are writing into an array, and the content, to depend
of the process, is the PID of the writer.


#### Expected result

The latest process must print an array with PID of child and the parent.


### Message Queue (race condition in reading) ([`mq.c`](mq.c))

This is not related to CPU caches. It's just to test a race condition.

To test race conditions with message queues, the protocol is the following: Some
process will write into the queue, others are reading. There are 2 readers and 2
writers per priority.

*Actually, docs say it is safe to this kind of races*


#### Expected result

Each message is read only twice per priority.


More infos
----------

* [`man shm_overview`](http://man7.org/linux/man-pages/man7/shm_overview.7.html)
* [`man sem_overview`](http://man7.org/linux/man-pages/man7/sem_overview.7.html)
* [`man mq_overview`](http://man7.org/linux/man-pages/man7/mq_overview.7.html)
* [FR] http://fr.lang.free.fr/cours/IPC_Csyst_v1.0.pdf
* [EN] https://www.enseignement.polytechnique.fr/informatique/INF583/INF583_7.pdf
* [EN] https://www.enseignement.polytechnique.fr/informatique/INF583/INF583_8.pdf
* [EN] https://www.cs.cmu.edu/afs/cs/academic/class/15213-s13/www/lectures/26-parallelism.pdf
